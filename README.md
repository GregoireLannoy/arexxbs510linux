# arexxbs510linux

arexxbs510linux is a simple program for GNU/Linux users to get data from an
Arexx BS510.

arexxbs510linux also generate HTML5 graph with data. The graph could be export
PNG, SVG, JPG, PDF.

## Output format

* Graph with HTML5/JS and export to PNG, SVG, JPG, PDF
* JSON
* Python dictionnary

The output could be write in a file or in standard output.

## Usage

1. First, catch data using arexxbs510linux/getdata.sh  
    `cd arexxbs510linux`  
    `./getdata.sh`

1. Wait until the first HTTP request to exit with `Ctrl+C`

1. Execute `python arexxbs510.py`

## Options

`-o file` write data in file. Output format is determinate by file extension.
Available extension are .json .python .html

`-f format` set output format. Available format are json, python and html.

`-v` tells the program to be verbose. Write some interisting stuff
(useful for debug)

`-i` set the file to use to extract data. By default the file is `log`

Default option are:

* Output in standard output (console)
* JSON format
* `log` as input file
* Not verbose

*The python format is mainly for debug*
