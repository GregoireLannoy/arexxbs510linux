# coding: utf-8
# Code by Grégoire Lannoy
# gregoire.lannoy@gmail.com

import sys
import os.path
import json
import subprocess
import time


def main():

    # Defaults variables
    input_type = 'intern'    # default input type (could be intern)
    log_file = 'log'         # default file with raw data
    verbose = False          # give some informations about data
    output_type = 'standard' # set output type
    output_format = 'json'   # set JSON as default output format

    # Available output
    available_output = ('file', 'standard')

    # Available format
    available_format = ('json', 'python', 'html')

    # Get some arguments
    if '-i' in sys.argv:
        if len(sys.argv) > sys.argv.index('-i') + 1:
            log_file = sys.argv[sys.argv.index('-i') + 1]
            input_type = 'file'
        else:
            print('No argument found for -i option')
            return

    if '-v' in sys.argv:
        verbose = True

    if '-o' in sys.argv:
        if len(sys.argv) > sys.argv.index('-o') + 1:
            output_file = sys.argv[sys.argv.index('-o') + 1]
            detected_format = output_file[output_file.find('.') + 1:]
            if detected_format in available_format:
                output_type = 'file'
                output_format = detected_format
            else:
                print(detected_format, 'is not a valid format')
                return
        else:
            print('No argument found for -o option')
            return

    if '-f' in sys.argv:
        if len(sys.argv) > sys.argv.index('-f') + 1:
            output_format = sys.argv[sys.argv.index('-f') + 1]
            if format not in available_format:
                print(format, 'format is not available')
                return
        else:
            print('No argument found for -f option')
            return

    # Get raw data
    if input_type == 'file':
        raw_data = open_file(log_file, 'r')

    elif input_type == 'intern':
        if catch_rf_usb_http_output():
            raw_data = open_file('log', 'r')
        else:
            print('Can\'t catch data from rf_usb_http')
            return

    if not raw_data:
            print('No data')
            return

    # Parse data
    data = parse_data(raw_data)

    # Return specific information if verbose True
    if verbose:
        print('Raw data file is :', log_file)
        print('Basetime is :', data['basetime'])
        print(
            'IDs :', ', '.join(data['IDs']), '(' + str(len(data['IDs'])) + ')')
        print('Output :', output_type)
        print('Format :', output_format)

    # get result as text
    if output_format == 'python':
        text = data

    elif output_format == 'json':
        text = json.dumps(data, indent=2, sort_keys=True)

    elif output_format == 'html':
        text = make_HTML(data)

    # output
    if output_type == 'standard':
        print(text)

    elif output_type == 'file':
        write_file(output_file, text, 'w')


def parse_data(raw_data):
    data = {'IDs': [], 'basetime': 0, 'measure': {}}
    for line in raw_data:
        if line[0:7] == 'NEGLECT' or line[0:6] == 'RECENT':
            id = int(line[line.find('id') + 3:line.find('id') + 7])
            measure_time = int(
                line[line.find('time') + 5:line.find('time') + 15]) * 1000
            value = round( float(line[line.find('value') + 6:-2]), 1)

            measure = [measure_time, value]

            if id not in data['IDs']:
                data['IDs'].append(id)
                data['measure'][id] = [measure]

            if measure[0]-10*60*1000 > data['measure'][id][-1][0]:
                data['measure'][id].append(measure) 

    return data


def open_file(file_to_open, mode):
    if os.path.isfile(file_to_open):
        f = open(file_to_open, mode)
    else:
        f = False

    return f


def catch_rf_usb_http_output():
    cmd = subprocess.Popen(
        ['script', '-c', '../rf_usb_http/rf_usb_http.elf -v ../rf_usb_http/rulefile.txt'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, universal_newlines=True)
    data = ''
    start_time = time.time()
    for line in iter(cmd.stdout.readline, ''):
        if line[0:7] == 'NEGLECT' or line[0:6] == 'RECENT':
            data += line

        if line[0:1] == '&':
            break

        if (time.time() - start_time) > 10 and len(data) == 0:
            break

    if len(data) == 0:
        return False

    else:
        write_file('log', data, 'a')
        return True


def make_HTML(data):
    with open('base.html', 'r') as f:
        base = f.read()
        rawDataInsertPosition = base.find('rawData =') + len('rawData =') + 1
        HTML = base[:rawDataInsertPosition] + \
            json.dumps(data) + base[rawDataInsertPosition:]

    return HTML


def write_file(outputFile, text, mode):
    with open(outputFile, mode) as f:
        f.write(text)

if __name__ == "__main__":
    main()
